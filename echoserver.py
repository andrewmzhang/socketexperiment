import socket

HOST = socket.gethostname()
PORT = 1080

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()
print 'Connected by', addr
while True:
	try:
		data = conn.recv(1024)
		print "data is: ", data
		
		conn.sendall(data)
		s.listen(1)
	except:
		break
conn.close()
s.close()
print 'Clean exit'
